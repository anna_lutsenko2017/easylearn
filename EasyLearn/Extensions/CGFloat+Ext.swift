//
//  CGFloat+Ext.swift
//  EasyLearn
//
//  Created by Anna on 31.07.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import UIKit

extension CGFloat {
    static func randomNumber() -> CGFloat {
        return CGFloat(Float(arc4random()) / Float(UINT32_MAX))
    }
}
