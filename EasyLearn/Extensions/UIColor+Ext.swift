//
//  UIColor+Ext.swift
//  EasyLearn
//
//  Created by Anna on 31.07.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import UIKit

extension UIColor {
    static func getBackgroundColor() -> UIColor {
        let red = CGFloat.randomNumber()
        let green = CGFloat.randomNumber()
        let blue = CGFloat.randomNumber()
        let bgColor = UIColor(red: red, green: green, blue: blue, alpha: 0.5)
        return bgColor
    }
}
