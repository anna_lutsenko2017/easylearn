//
//  GroupViewController.swift
//  EasyLearn
//
//  Created by Anna on 31.07.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import UIKit

class GroupViewController: UIViewController, UICollectionViewDelegate,  UICollectionViewDataSource {
    
//    UICollectionViewController
    @IBOutlet weak var groupCollectionView: UICollectionView!
//    var longPressGesture : UIGestureRecognizer
    var array = ["Noun", "Adjective", "Verb", "Time, calendar", "Animals", "Vegetables", "Relatives","Parts of speech","Feelings, sensations","Food and drink", "Body parts", "Fruits, berries, nuts"]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        createItemWith(size: 2)
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongGesture(gesture:)))
        
        self.groupCollectionView.addGestureRecognizer(longPressGesture)
    }
    
    func handleLongGesture(gesture: UILongPressGestureRecognizer) {

        switch(gesture.state) {
            
        case UIGestureRecognizerState.began:
            guard let selectedIndexPath = groupCollectionView.indexPathForItem(at: gesture.location(in: groupCollectionView)) else {
                break
            }
            groupCollectionView.beginInteractiveMovementForItem(at: selectedIndexPath)
        case UIGestureRecognizerState.changed:
            groupCollectionView.updateInteractiveMovementTargetPosition(gesture.location(in: gesture.view!))
        case UIGestureRecognizerState.ended:
            groupCollectionView.endInteractiveMovement()
        default:
            groupCollectionView.cancelInteractiveMovement()
        }
    }

    func createItemWith(size: CGFloat) {
        let spacing : CGFloat = 5
        let itemSize = UIScreen.main.bounds.width/size - spacing
        
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsetsMake(10, size, 10, size)
        layout.itemSize = CGSize(width: itemSize, height: itemSize)
        
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = spacing
        
        groupCollectionView.collectionViewLayout = layout
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return array.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = groupCollectionView.dequeueReusableCell(withReuseIdentifier: "reuseIdentifier", for: indexPath) as! GroupCollectionViewCell
        cell.labelTitleGroup.text = array[indexPath.row]
        cell.backgroundColor = UIColor.getBackgroundColor()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        
        let temp = self.array.remove(at: sourceIndexPath.row)
        self.array.insert(temp, at: destinationIndexPath.row)
    }

    

}
